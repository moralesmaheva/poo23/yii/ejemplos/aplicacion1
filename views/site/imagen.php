<?php

use yii\helpers\Html;
?>
<h2>
    Esta vista nos muestra una imagen
</h2>



<?= Html::img("@web/imgs/{$foto}", // ruta a la imagen con helper
[
    //atributos de la etiqueta img
    'alt' => 'imagen',
    'class'=> 'rounded img-thumbnail col-6',
]) ?>


