<?php

use yii\helpers\Html;
?>
<h2>
    Imagenes
</h2>

<?php
echo Html::img('@web/imgs/1.jpg', ['class' => 'rounded img-thumbnail col-3']);
echo Html::img('@web/imgs/2.jpg', ['class' => 'rounded img-thumbnail col-3']);
echo Html::img('@web/imgs/3.jpg', ['class' => 'rounded img-thumbnail col-3']);
echo Html::img('@web/imgs/4.jpg', ['class' => 'rounded img-thumbnail col-3']);
?>