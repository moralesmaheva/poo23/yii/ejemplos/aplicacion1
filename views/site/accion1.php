<?php

use yii\helpers\Html;
?>
<h2>Suma</h2>
<ul>
    <li><?= $numeros[0] ?></li>
    <li><?= $numeros[1] ?></li>
</ul>
<div>
    La suma es <?= $suma ?>
</div>

<h2>Datos</h2>
<?= Html::ul($datos) ?>
<div>
    El numero mayor es <?= $mayor ?>
</div>


<h2>Numero de veces que se repite una vocal</h2>
<div>
    El texto es: <?= $texto ?>
</div>
<div>
    El numero de veces que se repite la vocal 'e' es <?= $ne ?>
</div>