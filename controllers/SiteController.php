<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //accion que se ejecute por defecto
        return $this->render('index');
    }

    public function actionPrueba()
    {
        return $this->render('prueba', [
            'valor' => 'Aqui estoy',
        ]);
    }

    public function actionMensaje()
    {
        return $this->render('mostrarMensaje');
    }


    public function actionImagen($id = 0)
    {
        //para mostrar solo una imagen y pasale como argumento
        //creo un array con las imagenes
        $imagenes = [
            'foto1.jpg',
            'foto2.jpg',
            'foto3.jpg',
            'foto4.jpg',
        ];
        //le pasamos el id de la imagen
        return $this->render('imagen', [
            'foto' => $imagenes[$id]
        ]);
    }


    public function actionDias()
    {
        //introduzco los dias en un array
        $dias = [
            'Lunes',
            'Martes',
            'Miercoles',
            'Jueves',
            'Viernes',
            'Sabado',
            'Domingo'
        ];
        return $this->render('dias', $dias = ['dias' => $dias]);
    }

    public function actionMeses()
    {
        //introduzco los mese en un array
        $datos = [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        ];
        return $this->render('meses', $datos = ['meses' => $datos]);
    }


    public function actionImagenes()
    {
        return $this->render('imagenes');
    }


    public function actionAccion1()
    {
        //recoge los datos de un modelo o de un formulario y los trata
        //devolver los resultados o datos a la vista

        //suma dos numeros
        $numero = 1;
        $numero1 = 2;
        $suma = $numero + $numero1;

        //mayor de un array
        $datos = [1, 2, 3, 4, 5];
        $mayor = max($datos);

        //cuantas letras e hay
        $texto = "Ejemmplo de clase";
        $ne = substr_count(strtolower($texto), "e");

        //mandamos los datos a la vista
        return $this->render('accion1', [
            'numeros' => [$numero, $numero1],
            'suma' => $suma,
            'datos' => $datos,
            'mayor' => $mayor,
            'texto' => $texto,
            'ne' => $ne
        ]);
    }
}
